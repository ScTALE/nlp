import os

pathO = 'temp/Original'
pathMod = 'temp/Mod'

#if a ' isn't near its word, there can be some mistakes:
#example: haven '
#If you change the input documents, you should first put them in pathO, run this file and then 'move.sh'.
#Doing that, in pathMod there will be the file with ' fixed and with move.sh these files will be moved in InputDocuments

for filename in os.listdir(pathO):
    with  open(pathO + "/" + filename, 'r') as input, open(pathMod + "/"+ filename, 'w') as textfile_new:
        for line in input:
            new_line = line
            new_line=new_line.replace(" '","'")
            textfile_new.write(new_line)
print("'Fixed")
print("Now manually execute temp/move.sh please")