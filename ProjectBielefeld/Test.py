import csv
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
from sklearn.metrics import jaccard_similarity_score

dict_name="dict.csv"
specific_dict={}

list1 = []
list2 = []

modelName = "Model"
vectorName = "WordVectors"

modelNameBig = "ModelBig"
vectorNameBig = "WordVectorsBig"

neighbour1 = {}
neighbour2 = {}

neighbour_to_analyze = {}

neighbour1BIG = {}
neighbour2BIG = {}
neighbour_to_analyzeBIG = {}

#load dictionary from disk
with open(dict_name, 'rt') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        word = row[0]
        word = word.replace("'","")
        word = word.replace("[","")

        number = row[1]
        number = number.replace("'","")
        number = number.replace("]","")

        specific_dict[word] = number
        list1.append(word+"_1")
        list2.append(word+"_2")


model = Word2Vec.load(modelName)
word_vectors = KeyedVectors.load_word2vec_format(vectorName+".bin", binary=True)
print("Small embedding loaded")

modelBig = Word2Vec.load(modelNameBig)
word_vectorsBig = KeyedVectors.load_word2vec_format(vectorNameBig+".bin", binary=True)
print("Big embedding loaded")

for word1 in list1:
    try:
        neighbour1[word1] = model.wv.most_similar(word1)
    except:
        # the word is present in doc2 but not in doc1 or the word is not a noun
        pass
    try:
        neighbour1BIG[word1] = modelBig.wv.most_similar(word1)
    except:
        pass

for word2 in list2:
    try:
        neighbour2[word2] = model.wv.most_similar(word2)
    except:
        # the word is present in doc1 but not in doc2 or the word is not a noun
        pass
    try:
        neighbour2BIG[word2] = modelBig.wv.most_similar(word2)
    except:
        pass

for word in specific_dict:
    word1 = word+"_1"
    word2 = word+"_2"
    if((neighbour1.get(word1.lower()) is not None) and (neighbour2.get(word2.lower()) is not None)):
        neighbour_to_analyze[word1]=neighbour1.get(word1)
        neighbour_to_analyze[word2]=neighbour2.get(word2)

    if ((neighbour1BIG.get(word1.lower()) is not None) and (neighbour2BIG.get(word2.lower()) is not None)):
        neighbour_to_analyzeBIG[word1] = neighbour1BIG.get(word1)
        neighbour_to_analyzeBIG[word2] = neighbour2BIG.get(word2)


neighbourToAnalyzeName = "neighbour_to_analyze.csv"
neighbourToAnalyzeNameBIG = "neighbour_to_analyzeBIG.csv"
resultName = "Result.txt"
resultNameBIG = "ResultBIG.txt"

with open(neighbourToAnalyzeName,'w') as newfile:
    for el in neighbour_to_analyze:
        newfile.write(el + " :neighbour: ")
        newfile.write(str(neighbour_to_analyze.get(el)))
        newfile.write("\n\n")
print("Neighbour to analyze created")

with open(neighbourToAnalyzeNameBIG,'w') as newfile:
    for el in neighbour_to_analyzeBIG:
        newfile.write(el + " :neighbour: ")
        newfile.write(str(neighbour_to_analyzeBIG.get(el)))
        newfile.write("\n\n")
print("BIG Neighbour to analyze created")

def similarity(list1,list2):
    l1 = [i[0] for i in list1]
    l2 = [j[0] for j in list2]
    j = jaccard_similarity_score(l1, l2)
    return j

#in neighbour_to_analyze there is always word_1 followed by word_2 or word_2 followed by word_1
counter=0

with open(resultName,'w') as newfile:
    for el in neighbour_to_analyze:
        if(counter==0):
            counter=1
            neig1 = neighbour_to_analyze.get(el)
            if("_1" in el):
                word=el.replace("_1","_2")
            else:
                word=el.replace("_2","_1")
            neig2 = neighbour_to_analyze.get(word)
            s = similarity(neig1,neig2)

            word=word.replace("_1","")
            word=word.replace("_2","")
            newfile.write(word+" has similarity "+str(s)+"\n")

        else:
            counter=0
print("Result created")

with open(resultNameBIG,'w') as newfile:
    for el in neighbour_to_analyzeBIG:
        if(counter==0):
            counter=1
            neig1 = neighbour_to_analyzeBIG.get(el)
            if("_1" in el):
                word=el.replace("_1","_2")
            else:
                word=el.replace("_2","_1")
            neig2 = neighbour_to_analyzeBIG.get(word)
            s = similarity(neig1,neig2)

            word=word.replace("_1","")
            word=word.replace("_2","")
            newfile.write(word+" has similarity "+str(s)+"\n")

        else:
            counter=0
print("BIG result created")
