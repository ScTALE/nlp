import subprocess
import csv
import os
import time
import re
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk import word_tokenize

import codecs
from statistics import mean
import math


#path
pathOriginal = 'InputDocuments'
pathOutput = 'output'
pathTagged = 'tagged'
pathNew = 'nounsTaggedDocs'
pathBig = 'big_for_embeddings'
pathBigBig = 'big_for_embeddings_big'
pathWikiReplaced = 'WikipediaReplaced'

#dictionary with specific words
specific_words_dict={}

specific_threshold = 0.3

fullDictName = 'full_dict.csv'
sortedDicName = 'dict.csv'
unsortedDicName = 'uDict.csv'


def conversion_for_threshold(n,mini,maxi):
    n=(n-mini)/(maxi-mini)
    n=n*100
    return n

#tf-idf implementation
#it creates the dict.csv file with all the words and their frequency

answer = raw_input("Run rf_idf and overwrite data? (y or n)")
if ((answer == 'yes') or (answer == 'y')):
    print("Calling Tf-idf algorithm on InputDocuments folder")
    # subprocess.call(['./tf_idf.sh'])
    with codecs.open(pathOriginal+"/HarryPotter1.txt",'r','utf-8') as i1, codecs.open(pathOriginal+"/HarryPotter7.txt",'r','utf-8') as i2, codecs.open(pathOriginal+"/Wikipedia.txt",'r','utf-8') as i3:
        start_time = time.time()
        # your corpus
        t1=i1.read().replace('\n',' ')
        t2=i2.read().replace('\n',' ')
        t3=i3.read().replace('\n',' ')

        text = [t1,t2,t3]
        # word tokenize and stem
        text = [" ".join(word_tokenize(txt.lower())) for txt in text]
        vectorizer = TfidfVectorizer()
        matrix = vectorizer.fit_transform(text).todense()
        # transform the matrix to a pandas df
        matrix = pd.DataFrame(matrix, columns=vectorizer.get_feature_names())
        # sum over each document (axis=0)
        top_words = matrix.sum(axis=0).sort_values(ascending=False)

        text = [" ".join(word_tokenize(t3.lower()))]
        matrix = vectorizer.fit_transform(text).todense()
        matrix = pd.DataFrame(matrix, columns=vectorizer.get_feature_names())
        top_words3 = matrix.sum(axis=0).sort_values(ascending=False)
        # Mask out words in t3
        mask = ~top_words.index.isin(top_words3.index)
        # Filter those words from top_words
        top_words = top_words[mask]

        elapsed_time = time.time() - start_time
        print("Tf-Idf completed in " + str(elapsed_time) + " secondi, " + str(elapsed_time / 60) + " minuti")
        top_words.to_csv(fullDictName, index=True, float_format="%f",encoding="utf-8")
        print("Specific words dictionary created")
else:
    print("Hoping that dict.csv already exists")

#calculate the threshold
#specific_threshold = find_threshold()
#print("The threshold is "+str(specific_threshold))


#minmax for threshold
data = []
with open(fullDictName, 'rt') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        number = float(row[1])
        data.append(number)
mini = min(data)
maxi = max(data)

#fullfill the dictionary with the words over the threshold
#wikipedia is needed to create the correct precision of the words,
#wikipedia terms will be discarded during the evaluation phase

with open(fullDictName, 'rt') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        number = float(row[1])
        check= conversion_for_threshold(number,mini,maxi)
        if (check > specific_threshold):
        #if(number > specific_threshold):
            specific_words_dict[row[0]] = number
print("Specific words dictionary loaded")

#create a new csv with only the words over the threshold
with open(unsortedDicName,'w') as csvfile:
    for entry in specific_words_dict:
        csvfile.write(entry+",")
        csvfile.write(str(specific_words_dict.get(entry)))
        csvfile.write("\n")

#sort the file
with open(unsortedDicName,'r') as csvfile:
    csvfilereader = csv.reader(csvfile, delimiter=',')
    sortedlist = sorted(csvfilereader, key=lambda r: float(r[1]), reverse=True)
    with open(sortedDicName,'w') as newFile:
        for el in sortedlist:
            newFile.write(str(el)+"\n")

#postagger.sh creates a directiory called tagged and it puts inside of it a txt for each file in
#InputDocumts. Each of these txt files has the words tagged
if os.listdir(pathTagged) == []:
    print("Calling postagger on "+ pathOriginal + "folder")
    subprocess.call(['./postagger.sh'])
else:
    answer = raw_input("Run postagger and overwrite data? (y or n)")
    if ((answer == 'yes') or (answer == 'y')):
        print("Calling postagger on " + pathOriginal + "folder")
        subprocess.call(['./postagger.sh'])
    else:
        print "Tagged folder (postagger) already exists"

#for each file in InputDocuments, if that file has already been tagged, it is created a new one inside the folder nounds tagged.
#if a word is a noun and it is in the dictionary (its frequency value is over the threshold),
#that word is replaced with word_numberOfTheDocument
answer = raw_input("Replace nouns and overwrite data in nounsTaggedDocs? (y or n)")
if((answer == 'yes') or (answer == 'y')):
    counter = 1 #for doc 1 and doc 2
    for filename in os.listdir(pathOriginal):
        try:
            with  open(pathTagged+"/"+filename.split(".")[0]+"_tagged.txt",'r') as taggedfile,open(pathNew+"/nounsTagged_"+filename,'w') as textfile_new:
                for line in taggedfile:
                    new_line = line
                    for word in line.split():
                        sp = word.split('_')    #0 is the word, 1 is the tag
                        word = sp[0]
                        if((sp[1]=="NN") or (sp[1]=="NNS") or (sp[1]=="NNP") or (sp[1]=="NNPS")):
                            if(specific_words_dict.get(word.lower()) is not None):
                                textfile_new.write(word + "_"+str(counter)+" ")
                            else:
                                textfile_new.write(word+" ")
                        else:
                            textfile_new.write(word+" ")
                    textfile_new.write("\n")

            counter=counter+1
        except:
            print("The file "+filename+" has not been tagged")

    print("Replacement of nouns completed")

else:
    print("Using existing documents in nounsTaggedDocs")


answer = raw_input("Would you like to replace terms in Wikipedia corpus? (y or n)")
if((answer == 'yes') or (answer == 'y')):
    fnamef="Wikipedia.txt"
    fname="Wikipedia"
    with open(pathOriginal+"/"+fnamef,'r') as original, \
        open(pathWikiReplaced+"/replaced_"+fname+"_1.txt",'w') as mod1, \
            open(pathWikiReplaced+"/replaced_"+fname+"_2.txt",'w') as mod2:
        data = original.read()
        data_1=data
        data_2=data
        for word in specific_words_dict:
            data_1 = re.sub(r"\b{0}\b".format(word), word+"_1", data_1)
            data_2 = re.sub(r"\b{0}\b".format(word), word+"_2", data_2)
        mod1.write(data_1 + r"\n")
        mod2.write(data_2 + r"\n")
        print("Wikipedia terms replaced")

else:
    print("Hoping to work with Wikipedia terms replaced")

#each file in nounsTaggedDocs is merged inside a single long one
with open(pathBig+"/longText.txt",'w') as longFile:
    for filename in os.listdir(pathNew):
        with open(pathNew+"/"+filename,'r') as textfile:
            for line in textfile:
                longFile.write(line.lower())
print("Long text created (for smallEmbedding)")

#longText.txt + 2 wikipedia files are merged
with open(pathBigBig+"/BigTextBig.txt",'w') as bigFile:
    for filename in os.listdir(pathBig):
        with open(pathBig+"/"+filename,'r') as textfile:
            for line in textfile:
                bigFile.write(line.lower())
    for filename in os.listdir(pathWikiReplaced):
        with open(pathWikiReplaced+"/"+filename,'r') as textfile:
            for line in textfile:
                bigFile.write(line.lower())

print("Big text created")

#creation of a new embedding or use of an existing one
model = ""
word_vectors = ""
modelName = "Model"
vectorName = "WordVectors"

modelBig = ""
word_vectorsBig = ""
modelNameBig = "ModelBig"
vectorNameBig = "WordVectorsBig"

answer = raw_input("Overwrite wordembedding (it will take a lot of time)?  (y or n)")
if((answer == 'yes') or (answer == 'y')):
    answer2 = raw_input("1 for overwrite smallEmbedding\n2 for overwrite bigEmbedding")
    if(answer2 == '1'):
        print("Overwriting smallEmbedding")
        try:
            os.remove(vectorName+".bin")
            os.remove(vectorName + ".txt")
            os.remove(vectorName)
        except:
            print("(Nothing to overwrite)")
            print("Creating a new embedding")
        with open(pathBig+"/longText.txt",'r') as longFile:
            sentences = []
            single= []
            for line in longFile:
                for word in line.split(" "):
                    single.append(word)
                sentences.append(single)

        start_time = time.time()
        model = Word2Vec(sentences,workers=4, window=5)

        #The word vectors are stored in a KeyedVectors instance in model.wv.
        #This separates the read-only word vector lookup operations in KeyedVectors from the training code in Word2Vec:
        model.save(modelName)
        model.wv.save_word2vec_format(vectorName+".bin",binary=True)
        model.wv.save_word2vec_format(vectorName+".txt", binary=False)
        model.wv.save(vectorName)

        word_vectors = model.wv

        elapsed_time = time.time() - start_time
        print("Small Embedding made in "+str(elapsed_time/60)+" minutes")
    else:
        if(answer2 == '2'):
            print("Overwriting BigEmbedding")
            try:
                os.remove(vectorNameBig + ".bin")
                os.remove(vectorNameBig + ".txt")
                os.remove(vectorNameBig)
            except:
                print("(Nothing to overwrite)")
                print("Creating a new embedding")
            with open(pathBigBig + "/BigTextBig.txt", 'r') as BigFile:
                sentences = []
                single = []
                for line in BigFile:
                    for word in line.split(" "):
                        single.append(word)
                    sentences.append(single)

            start_time = time.time()
            modelBig = Word2Vec(sentences, workers=4, window=5)

            # The word vectors are stored in a KeyedVectors instance in model.wv.
            # This separates the read-only word vector lookup operations in KeyedVectors from the training code in Word2Vec:
            modelBig.save(modelNameBig)
            modelBig.wv.save_word2vec_format(vectorNameBig + ".bin", binary=True)
            modelBig.wv.save_word2vec_format(vectorNameBig + ".txt", binary=False)
            modelBig.wv.save(vectorNameBig)

            word_vectorsBig = modelBig.wv

            elapsed_time = time.time() - start_time
            print("Big Embedding made in " + str(elapsed_time / 60) + " minutes, "+str(elapsed_time/3600)+" hours")
        else:
            print("Nothing has been overwritten")

else:
    print("Using existing embedding")
    model = Word2Vec.load(modelName)
    word_vectors = KeyedVectors.load_word2vec_format(vectorName+".bin", binary=True)
    print("Small embedding loaded")
    modelBig = Word2Vec.load(modelNameBig)
    word_vectorsBig = KeyedVectors.load_word2vec_format(vectorNameBig+".bin", binary=True)
    print("Big embedding loaded")

